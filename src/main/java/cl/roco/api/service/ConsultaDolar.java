package cl.roco.api.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.text.ParseException;

/**
 *
 * @author juan.roco
 */
@Path("/dolar/{fecha}")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConsultaDolar {

  public static String fechaIn;
  public static String valor = "dolar";
  public static int dia0;

  @GET
  public Response getDolar(@PathParam("fecha") String fecha) {

    String dia = fecha.substring(0, 2);
    String mes = fecha.substring(2, 4);
    String anio = fecha.substring(4, 8);

    fechaIn = (anio + mes + dia);

    getFechaValida();

    return Response.ok(valor).build();

  }

  public Date getFechaActual() {
    Date ahora = new Date();
    SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");
    formateador.format(ahora);
    return ahora;
  }

  public void getFechaValida() {
    Date fechaget = new Date();
    SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");
    formateador.format(fechaget);
    

    try {
      fechaget = formateador.parse(fechaIn);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    if (1 == fechaget.compareTo(getFechaActual())) {
      fechaget = getFechaActual();
      fechaIn = formateador.format(fechaget); 
    }

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fechaget);
    getDolarHoy();
    while (valor.contains("No hay datos disponibles para los parametros ingresados")) {
      calendar = Calendar.getInstance();
      calendar.setTime(fechaget);
      calendar.add(Calendar.DAY_OF_YEAR, -1);
      fechaget = calendar.getTime();
      fechaIn = formateador.format(fechaget);
      getDolarHoy();
    }
    

  }


  public void getDolarHoy() {
    String fecha = fechaIn;
    String dia = fecha.substring(6, 8);
    String mes = fecha.substring(4, 6);
    String anio = fecha.substring(0, 4);

    fecha = anio + "/" + mes + "/dias/" + dia;

    Client client = ClientBuilder.newClient();

    String url = ("https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + fecha + "?apikey=4e4d89ab1ef7d9d641c0ade64686d903453635e0&formato=json");
        //.queryParam("apikey", "4e4d89ab1ef7d9d641c0ade64686d903453635e0").queryParam("formato", "json");

     Response respuesta = client.target(url).request().get();
     valor = respuesta.readEntity(String.class);
  }

}
